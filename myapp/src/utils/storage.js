function getUsers() {
  const storedUsers = localStorage.getItem("users");
  return storedUsers ? JSON.parse(storedUsers) : [];
}

function storeUsers(users) {
  localStorage.setItem("users", JSON.stringify(users));
}

export { getUsers, storeUsers };
