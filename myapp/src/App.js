import "./App.css";

import React from "react";

import { BrowserRouter, Route, Routes } from "react-router-dom";

import About from "./component/About";
import Hobby from "./component/Hobby";
import Home from "./component/Home";
import UserDetail from "./component/UserDetail";

function App() {
  return (
    <BrowserRouter>
      <Routes>
        <Route path="/" element={<Home />} />
        <Route path="/view/:index" element={<UserDetail />} />
        <Route path="/about" element={<About />} />
        <Route path="/hobby" element={<Hobby />} />
      </Routes>
    </BrowserRouter>
  );
}

export default App;
