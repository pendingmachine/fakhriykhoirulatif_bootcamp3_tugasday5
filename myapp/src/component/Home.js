import Button from "@mui/material/Button";
import TextField from "@mui/material/TextField";
import React, { useState } from "react";

import ButtonAppBar from "./ButtonAppBar";
import UserFormDialog from "./UserFormDialog";
import Users from "./Users";

import { getUsers, storeUsers } from "../utils/storage";

function Home() {
  const [searchText, setSearchText] = useState("");
  const [users, setUsers] = useState(getUsers());
  const [newUserData, setNewUserData] = useState({
    name: "",
    address: "",
    hobby: "",
  });
  const [currUserData, setCurrUserData] = useState({
    name: "",
    address: "",
    hobby: "",
  });
  const [currUserIndex, setCurrUserIndex] = useState(null);

  const [openAdd, setOpenAdd] = useState(false);
  const [openEdit, setOpenEdit] = useState(false);

  const getUsersByName = () => {
    return users.filter((user) => user.name.includes(searchText));
  };

  const openAddDialog = () => {
    setOpenAdd(true);
  };

  const updateNewUserData = (data) => {
    setNewUserData(data);
  };

  const closeAddDialog = () => {
    setOpenAdd(false);
  };

  const addUser = () => {
    const newUsers = users.concat(newUserData);
    setUsers(newUsers);
    setNewUserData({
      name: "",
      address: "",
      hobby: "",
    });
    closeAddDialog();

    storeUsers(newUsers);
  };

  const openEditUser = (index) => {
    setCurrUserData(users[index]);
    setCurrUserIndex(index);
    setOpenEdit(true);
  };

  const updateCurrUserData = (data) => {
    setCurrUserData(data);
  };

  const closeEditUser = () => {
    setCurrUserIndex(null);
    setCurrUserData({
      name: "",
      address: "",
      hobby: "",
    });
    setOpenEdit(false);
  };

  const editUser = () => {
    const newUsers = [...users];
    newUsers[currUserIndex] = currUserData;
    setUsers(newUsers);
    closeEditUser();

    storeUsers(newUsers);
  };

  return (
    <div>
      <ButtonAppBar>
        <Button variant="outlined" color="inherit" onClick={openAddDialog}>
          Add User
        </Button>
      </ButtonAppBar>
      <TextField
        type="search"
        label="Search"
        onChange={(e) => setSearchText(e.target.value)}
        sx={{
          margin: "16px",
          width: "200px",
        }}
      />
      <Users users={getUsersByName()} onUserEdit={openEditUser} />
      <UserFormDialog
        title="Add User"
        open={openAdd}
        data={newUserData}
        onChange={updateNewUserData}
        onClose={closeAddDialog}
        onSave={addUser}
      />
      <UserFormDialog
        title="Edit User"
        open={openEdit}
        data={currUserData}
        onChange={updateCurrUserData}
        onClose={closeEditUser}
        onSave={editUser}
      />
    </div>
  );
}

export default Home;
