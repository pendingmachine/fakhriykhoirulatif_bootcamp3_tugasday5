import React from "react";
import ButtonAppBar from "./ButtonAppBar";

function About() {
  return (
    <div>
      <ButtonAppBar />
      <div className="content-container">
        <h1>About</h1>
        <p style={{ textAlign: "center" }}>
          Aplikasi ini dapat digunakan untuk mamasukkan data-data user dan
          menampilkannya.
        </p>
      </div>
    </div>
  );
}

export default About;
