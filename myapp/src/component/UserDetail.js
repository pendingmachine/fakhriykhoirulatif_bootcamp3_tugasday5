import React from "react";
import { useLocation } from "react-router-dom";

import ButtonAppBar from "./ButtonAppBar";

function UserDetail() {
  const location = useLocation();
  const user = location.state.user;

  return (
    <div>
      <ButtonAppBar />
      <div className="content-container">
        <h1>{user.name}</h1>
        <p>Address: {user.address}</p>
        <p>Hobby: {user.hobby}</p>
      </div>
    </div>
  );
}

export default UserDetail;
