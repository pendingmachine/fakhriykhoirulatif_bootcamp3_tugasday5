import React from "react";

import ButtonAppBar from "./ButtonAppBar";

import { getUsers } from "../utils/storage";

function Hobby() {
  const users = getUsers();

  return (
    <div>
      <ButtonAppBar />
      <div className="content-container">
        <h1>Hobby</h1>

        <ul>
          {users.map((user) => (
            <li>{user.hobby}</li>
          ))}
        </ul>
      </div>
    </div>
  );
}

export default Hobby;
