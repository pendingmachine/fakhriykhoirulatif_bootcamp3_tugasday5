import AppBar from "@mui/material/AppBar";
import Box from "@mui/material/Box";
import Stack from "@mui/material/Stack";
import Toolbar from "@mui/material/Toolbar";
import Typography from "@mui/material/Typography";
import React from "react";

import { NavLink } from "react-router-dom";

function ButtonAppBar(props) {
  return (
    <Box sx={{ flexGrow: 1 }}>
      <AppBar position="static">
        <Toolbar>
          <Stack direction="row" spacing="16px" sx={{ flexGrow: 1 }}>
            <Typography variant="h6" component="div">
              My App
            </Typography>
            <NavLink
              to="/about"
              end
              className={({ isActive }) => (isActive ? "active-nav" : "")}
            >
              <Typography variant="h6" component="div">
                About
              </Typography>
            </NavLink>
            <NavLink
              to="/hobby"
              end
              className={({ isActive }) => (isActive ? "active-nav" : "")}
            >
              <Typography variant="h6" component="div">
                Hobby
              </Typography>
            </NavLink>
          </Stack>
          {props.children}
        </Toolbar>
      </AppBar>
    </Box>
  );
}

export default ButtonAppBar;
